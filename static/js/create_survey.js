const titleDivider = "||";
function updateStringComponents(_, options) {
    options.htmlElement.querySelectorAll('.sv-string-viewer').forEach((el) => {
        const text = el.innerText;
        if (text.indexOf(titleDivider) > -1) {
            const strings = text.split(titleDivider);
            el.title = strings[1];
            el.innerText = strings[0];
        }
    });
}

function create_new_survey(json2){
    console.log(json2)
    const survey = new Survey.Model(json2);
    document.getElementById("surveyElement").innerHTML="";
    // You can delete the line below if you do not use a customized theme
    survey.applyTheme(themeJson);
    survey.onComplete.add(function (sender) {
        console.log("obtaing results");
        console.log(JSON.stringify(sender.data));      
        var collected_answers={ "title":json2.pages[0].title, "user":localStorage.getItem('user_name') } 
        console.log("**************",collected_answers)
        collected_answers.answers=sender.data;
        console.log(JSON.stringify(collected_answers));
        document.querySelector('#surveyResult').textContent = "Result JSON:\n" + JSON.stringify(collected_answers, null, 3);
        ///////////////////////// Sending data to back end
        var send_data=JSON.stringify(collected_answers);
        console.log("sending data");
        var end_point = server_ip+"collect_data";
        
        const getData = async(url=end_point,api_method="POST",api_body=send_data) => {     
            var response;
            response = await fetch(url,{
                method: api_method,
                body: api_body, // string or object
                headers : { 
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                }
            });
            
            if (response.ok) {
                const raps = await response.json();
                console.log("raps:", raps)
                myresult = raps;
            }
            return (myresult);
  
            
        }
        getData().then(data => {
        console.log(data); alert(data.ack);} );
  
      });
    /*survey.onComplete.add((sender, options) => {
        console.log(JSON.stringify(sender.data, null, 3));
    });*/
    survey.onAfterRenderQuestion.add(updateStringComponents);

    $("#surveyElement").Survey({ model: survey });
}